/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support;

import com.rebuild.core.support.i18n.LanguageBundle;


public enum ConfigurationItem {

    
    SN, DBVer, AppBuild,

    
    CacheHost, CachePort, CacheUser, CachePassword,

    
    AppName("REBUILD"),
    LOGO,
    LOGOWhite,
    HomeURL("https://getrebuild.com/"),
    PageFooter,

    
    StorageURL, StorageApiKey, StorageApiSecret, StorageBucket,

    
    MailUser, MailPassword, MailAddr, MailName(AppName), MailCc, MailBcc,
    MailSmtpServer,

    
    SmsUser, SmsPassword, SmsSign(AppName),

    
    OpenSignUp(true),

    
    LiveWallpaper(true),
    
    CustomWallpaper,

    
    FileSharable(true),

    
    MarkWatermark(false),

    
    PasswordPolicy(1),

    
    RevisionHistoryKeepingDays(180),

    
    RecycleBinKeepingDays(180),

    
    DBBackupsEnable(true),

    
    DBBackupsKeepingDays(180),

    
    MultipleSessions(true),

    
    DefaultLanguage(LanguageBundle.SYS_LC),

    
    ShowViewHistory(true),

    
    LoginCaptchaPolicy(1),

    
    PasswordExpiredDays(0),

    
    AllowUsesTime,
    
    AllowUsesIp,
    
    Login2FAMode(0),

    
    MobileAppPath,

    
    DingtalkAgentid, DingtalkAppkey, DingtalkAppsecret, DingtalkCorpid,
    DingtalkPushAeskey, DingtalkPushToken,
    DingtalkSyncUsers(false),
    DingtalkSyncUsersRole,
    DingtalkRobotCode,
    DingtalkSyncUsersMatch("ID"),
    
    WxworkCorpid, WxworkAgentid, WxworkSecret,
    WxworkRxToken, WxworkRxEncodingAESKey,
    WxworkAuthFile,
    WxworkSyncUsers(false),
    WxworkSyncUsersRole,
    WxworkSyncUsersMatch("ID"),

    
    PortalBaiduMapAk,
    PortalOfficePreviewUrl,
    PortalUploadMaxSize(200),
    MobileNavStyle(34),
    PageMourningMode(false),
    
    
    DataDirectory,                  
    RedisDatabase(0),     
    MobileUrl,                      
    RbStoreUrl,                     
    TriggerMaxDepth,                
    SecurityEnhanced(false), 
    TrustedAllUrl(false), 
    LibreofficeBin,                 
    UnsafeImgAccess(false), 

    ;

    
    public static boolean inJvmArgs(String name) {
        return DataDirectory.name().equalsIgnoreCase(name)
                || RedisDatabase.name().equalsIgnoreCase(name)
                || MobileUrl.name().equalsIgnoreCase(name)
                || RbStoreUrl.name().equalsIgnoreCase(name)
                || TriggerMaxDepth.name().equalsIgnoreCase(name)
                || SecurityEnhanced.name().equalsIgnoreCase(name)
                || TrustedAllUrl.name().equalsIgnoreCase(name)
                || LibreofficeBin.name().equalsIgnoreCase(name)
                || UnsafeImgAccess.name().equals(name)
                || SN.name().equalsIgnoreCase(name);
    }

    private Object defaultVal;

    ConfigurationItem() {
    }

    ConfigurationItem(Object defaultVal) {
        this.defaultVal = defaultVal;
    }

    
    public Object getDefaultValue() {
        if (defaultVal != null && defaultVal instanceof ConfigurationItem) {
            return ((ConfigurationItem) defaultVal).getDefaultValue();
        }
        return defaultVal;
    }
}
