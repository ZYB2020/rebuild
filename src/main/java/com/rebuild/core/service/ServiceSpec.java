/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service;

import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;


public interface ServiceSpec {

    
    int getEntityCode();

    
    default Record createOrUpdate(Record record) {
        return record.getPrimary() == null ? create(record) : update(record);
    }

    
    Record create(Record record);

    
    Record update(Record record);

    
    int delete(ID recordId);
}
