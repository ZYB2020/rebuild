/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.general;

import cn.devezhao.bizz.privileges.Permission;
import cn.devezhao.bizz.privileges.impl.BizzPermission;
import cn.devezhao.commons.ReflectUtils;
import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.Filter;
import cn.devezhao.persist4j.PersistManagerFactory;
import cn.devezhao.persist4j.Query;
import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import com.rebuild.core.RebuildException;
import com.rebuild.core.metadata.DeleteRecord;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.metadata.MetadataSorter;
import com.rebuild.core.metadata.easymeta.DisplayType;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;
import com.rebuild.core.metadata.impl.EasyEntityConfigProps;
import com.rebuild.core.privileges.UserService;
import com.rebuild.core.privileges.bizz.InternalPermission;
import com.rebuild.core.privileges.bizz.User;
import com.rebuild.core.service.BaseService;
import com.rebuild.core.service.DataSpecificationException;
import com.rebuild.core.service.NoRecordFoundException;
import com.rebuild.core.service.approval.ApprovalHelper;
import com.rebuild.core.service.approval.ApprovalState;
import com.rebuild.core.service.general.recyclebin.RecycleStore;
import com.rebuild.core.service.general.series.SeriesGeneratorFactory;
import com.rebuild.core.service.notification.NotificationObserver;
import com.rebuild.core.service.query.QueryHelper;
import com.rebuild.core.service.trigger.ActionType;
import com.rebuild.core.service.trigger.RobotTriggerManager;
import com.rebuild.core.service.trigger.RobotTriggerManual;
import com.rebuild.core.service.trigger.RobotTriggerObserver;
import com.rebuild.core.service.trigger.TriggerAction;
import com.rebuild.core.service.trigger.TriggerWhen;
import com.rebuild.core.service.trigger.impl.AutoApproval;
import com.rebuild.core.support.i18n.Language;
import com.rebuild.core.support.task.TaskExecutors;
import com.rebuild.utils.CommonsUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


@Slf4j
@Service("rbGeneralEntityService")
public class GeneralEntityService extends ObservableService implements EntityService {

    
    public static final String HAS_DETAILS = "$DETAILS$";

    protected GeneralEntityService(PersistManagerFactory aPMFactory) {
        super(aPMFactory);

        addObserver(new NotificationObserver());
        addObserver(new RobotTriggerObserver());
    }

    @Override
    public int getEntityCode() {
        return 0;
    }

    
    
    @Override
    public Record createOrUpdate(Record record) {
        @SuppressWarnings("unchecked")
        final List<Record> details = (List<Record>) record.removeValue(HAS_DETAILS);

        final int rcm = GeneralEntityServiceContextHolder.getRepeatedCheckModeOnce();

        if (rcm == GeneralEntityServiceContextHolder.RCM_CHECK_MAIN
                || rcm == GeneralEntityServiceContextHolder.RCM_CHECK_ALL) {
            List<Record> repeated = getAndCheckRepeated(record, 20);
            if (!repeated.isEmpty()) {
                throw new RepeatedRecordsException(repeated);
            }
        }

        
        final boolean hasDetails = details != null && !details.isEmpty();

        
        boolean lazyAutoApproval4Details = false;
        boolean lazyAutoTransform4Details = false;
        boolean lazyHookUrl4Details = false;
        if (hasDetails) {

            

            TriggerAction[] hasAutoApprovalTriggers = getSpecTriggers(
                    record.getEntity(), ActionType.AUTOAPPROVAL, TriggerWhen.CREATE, TriggerWhen.UPDATE);
            lazyAutoApproval4Details = hasAutoApprovalTriggers.length > 0;
            
            if (!lazyAutoApproval4Details) {
                TriggerAction[] hasOnApprovedTriggers = getSpecTriggers(
                        record.getEntity().getDetailEntity(), null, TriggerWhen.APPROVED);
                lazyAutoApproval4Details = hasOnApprovedTriggers.length > 0;
            }
            
            if (lazyAutoApproval4Details) AutoApproval.setLazy();

            

            TriggerAction[] hasAutoTransformTriggers = getSpecTriggers(
                    record.getEntity(), ActionType.AUTOTRANSFORM, TriggerWhen.CREATE, TriggerWhen.UPDATE);
            lazyAutoTransform4Details = hasAutoTransformTriggers.length > 0;
            
            if (lazyAutoTransform4Details) CommonsUtils.invokeMethod("com.rebuild.rbv.trigger.AutoTransform#setLazy");

            

            TriggerAction[] hasHookUrlTriggers = getSpecTriggers(
                    record.getEntity(), ActionType.HOOKURL, TriggerWhen.CREATE, TriggerWhen.UPDATE, TriggerWhen.DELETE);
            lazyHookUrl4Details = hasHookUrlTriggers.length > 0;
            
            if (lazyHookUrl4Details) CommonsUtils.invokeMethod("com.rebuild.rbv.trigger.HookUrl#setLazy");
        }

        
        Map<Integer, ID> detaileds = new TreeMap<>();

        try {
            record = record.getPrimary() == null ? create(record) : update(record);
            if (!hasDetails) return record;

            

            final Entity detailEntity = record.getEntity().getDetailEntity();
            final String dtfField = MetadataHelper.getDetailToMainField(detailEntity).getName();
            final ID mainid = record.getPrimary();

            final boolean checkDetailsRepeated = rcm == GeneralEntityServiceContextHolder.RCM_CHECK_DETAILS
                    || rcm == GeneralEntityServiceContextHolder.RCM_CHECK_ALL;

            
            EntityService des = Application.getEntityService(detailEntity.getEntityCode());
            if (des.getEntityCode() == 0) des = this;

            
            for (int i = 0; i < details.size(); i++) {
                Record d = details.get(i);
                if (d instanceof DeleteRecord) {
                    des.delete(d.getPrimary());
                    detaileds.put(i, d.getPrimary());
                }
            }

            
            for (int i = 0; i < details.size(); i++) {
                Record d = details.get(i);
                if (d instanceof DeleteRecord) continue;

                if (checkDetailsRepeated) {
                    d.setID(dtfField, mainid);  

                    List<Record> repeated = des.getAndCheckRepeated(d, 20);
                    if (!repeated.isEmpty()) {
                        throw new RepeatedRecordsException(repeated);
                    }
                }

                if (d.getPrimary() == null) {
                    d.setID(dtfField, mainid);
                    des.create(d);
                } else {
                    des.update(d);
                }
                detaileds.put(i, d.getPrimary());
            }

            record.setObjectValue(HAS_DETAILS, detaileds.values());
            return record;

        } finally {
            if (lazyAutoApproval4Details) AutoApproval.executeLazy();
            if (lazyAutoTransform4Details) CommonsUtils.invokeMethod("com.rebuild.rbv.trigger.AutoTransform#executeLazy");
            if (lazyHookUrl4Details) CommonsUtils.invokeMethod("com.rebuild.rbv.trigger.HookUrl#executeLazy");
        }
    }

    
    @Override
    public Record create(Record record) {
        appendDefaultValue(record);
        checkModifications(record, BizzPermission.CREATE);
        setSeriesValue(record);
        return super.create(record);
    }

    
    @Override
    public Record update(Record record) {
        if (!checkModifications(record, BizzPermission.UPDATE)) {
            return record;
        }

        
        record = super.update(record);

        
        

        TriggerAction[] hasTriggers = getSpecTriggers(record.getEntity().getDetailEntity(),
                ActionType.GROUPAGGREGATION, TriggerWhen.UPDATE);
        if (hasTriggers.length > 0) {
            RobotTriggerManual triggerManual = new RobotTriggerManual();
            ID opUser = UserService.SYSTEM_USER;

            for (ID did : QueryHelper.detailIdsNoFilter(record.getPrimary())) {
                Record dUpdate = EntityHelper.forUpdate(did, opUser, false);
                triggerManual.onUpdate(
                        OperatingContext.create(opUser, BizzPermission.UPDATE, dUpdate, dUpdate));
            }
        }

        return record;
    }

    @Override
    public int delete(ID recordId) {
        return delete(recordId, null);
    }

    @Override
    public int delete(ID recordId, String[] cascades) {
        final ID currentUser = getCurrentUser();
        final RecycleStore recycleBin = useRecycleStore(recordId);

        int affected = this.deleteInternal(recordId);
        if (affected == 0) return 0;
        affected = 1;

        Map<String, Set<ID>> recordsOfCascaded = getCascadedRecords(recordId, cascades, BizzPermission.DELETE);
        for (Map.Entry<String, Set<ID>> e : recordsOfCascaded.entrySet()) {
            log.info("Cascading delete - {} > {} ", e.getKey(), e.getValue());

            for (ID id : e.getValue()) {
                if (Application.getPrivilegesManager().allowDelete(currentUser, id)) {
                    if (recycleBin != null) recycleBin.add(id, recordId);

                    int deleted = 0;
                    try {
                        deleted = this.deleteInternal(id);
                    } catch (DataSpecificationException ex) {
                        log.warn("Cannot delete {} because {}", id, ex.getLocalizedMessage());
                    } finally {
                        if (deleted > 0) {
                            affected++;
                        } else if (recycleBin != null) {
                            recycleBin.removeLast();  
                        }
                    }
                } else {
                    log.warn("No have privileges to DELETE : {} > {}", currentUser, id);
                }
            }
        }

        if (recycleBin != null) recycleBin.store();

        return affected;
    }

    
    protected int deleteInternal(ID recordId) throws DataSpecificationException {
        Record delete = EntityHelper.forUpdate(recordId, getCurrentUser());
        if (!checkModifications(delete, BizzPermission.DELETE)) {
            return 0;
        }

        
        Entity de = MetadataHelper.getEntity(recordId.getEntityCode()).getDetailEntity();
        if (de != null) {
            for (ID did : QueryHelper.detailIdsNoFilter(recordId)) {
                
                
                super.delete(did);
            }
        }

        return super.delete(recordId);
    }

    @Override
    public int assign(ID recordId, ID toUserId, String[] cascades) {
        final User toUser = Application.getUserStore().getUser(toUserId);
        final ID recordOrigin = recordId;
        
        if (MetadataHelper.getEntity(recordId.getEntityCode()).getMainEntity() != null) {
            recordId = QueryHelper.getMainIdByDetail(recordId);
        }

        final Record assignAfter = EntityHelper.forUpdate(recordId, (ID) toUser.getIdentity(), Boolean.FALSE);
        assignAfter.setID(EntityHelper.OwningUser, (ID) toUser.getIdentity());
        assignAfter.setID(EntityHelper.OwningDept, (ID) toUser.getOwningDept().getIdentity());

        
        Record assignBefore = null;

        int affected;
        if (toUserId.equals(Application.getRecordOwningCache().getOwningUser(recordId))) {
            
            log.debug("The record owner has not changed, ignore : {}", recordId);
            affected = 1;
        } else {
            assignBefore = countObservers() > 0 ? recordSnap(assignAfter, false) : null;

            delegateService.update(assignAfter);
            Application.getRecordOwningCache().cleanOwningUser(recordId);
            affected = 1;
        }

        Map<String, Set<ID>> cass = getCascadedRecords(recordOrigin, cascades, BizzPermission.ASSIGN);
        for (Map.Entry<String, Set<ID>> e : cass.entrySet()) {
            log.info("Cascading assign - {} > {}", e.getKey(), e.getValue());

            for (ID casid : e.getValue()) {
                affected += assign(casid, toUserId, null);
            }
        }

        if (countObservers() > 0 && assignBefore != null) {
            notifyObservers(OperatingContext.create(getCurrentUser(), BizzPermission.ASSIGN, assignBefore, assignAfter));
        }
        return affected;
    }

    @Override
    public int share(ID recordId, ID toUserId, String[] cascades, int rights) {
        final ID currentUser = getCurrentUser();
        final ID recordOrigin = recordId;
        
        if (MetadataHelper.getEntity(recordId.getEntityCode()).getMainEntity() != null) {
            recordId = QueryHelper.getMainIdByDetail(recordId);
        }

        boolean fromTriggerNoDowngrade = GeneralEntityServiceContextHolder.isFromTrigger(false);
        if (!fromTriggerNoDowngrade) {
            
            if ((rights & BizzPermission.UPDATE.getMask()) != 0) {
                if (!Application.getPrivilegesManager().allowUpdate(toUserId, recordId.getEntityCode()) 
                        || !Application.getPrivilegesManager().allow(currentUser, recordId, BizzPermission.UPDATE, true) ) {
                    rights = BizzPermission.READ.getMask();
                    log.warn("Downgrade share rights to READ({}) : {}", BizzPermission.READ.getMask(), recordId);
                }
            }
        }

        final String entityName = MetadataHelper.getEntityName(recordId);
        final Record sharedAfter = EntityHelper.forNew(EntityHelper.ShareAccess, currentUser);
        sharedAfter.setID("recordId", recordId);
        sharedAfter.setID("shareTo", toUserId);
        sharedAfter.setString("belongEntity", entityName);
        sharedAfter.setInt("rights", rights);

        Object[] hasShared = ((BaseService) delegateService).getPersistManagerFactory().createQuery(
                "select accessId,rights from ShareAccess where belongEntity = ? and recordId = ? and shareTo = ?")
                .setParameter(1, entityName)
                .setParameter(2, recordId)
                .setParameter(3, toUserId)
                .unique();

        int affected;
        boolean shareChange = false;
        if (hasShared != null) {
            if ((int) hasShared[1] != rights) {
                Record updateRights = EntityHelper.forUpdate((ID) hasShared[0], currentUser);
                updateRights.setInt("rights", rights);
                delegateService.update(updateRights);
                affected = 1;
                shareChange = true;
                sharedAfter.setID("accessId", (ID) hasShared[0]);

            } else {
                log.debug("The record has been shared and has the same rights, ignore : {}", recordId);
                affected = 1;
            }

        } else {
            
            if (log.isDebugEnabled()
                    && toUserId.equals(Application.getRecordOwningCache().getOwningUser(recordId))) {
                log.debug("Share to the same user as the record, ignore : {}", recordId);
            }

            delegateService.create(sharedAfter);
            affected = 1;
            shareChange = true;
        }

        Map<String, Set<ID>> cass = getCascadedRecords(recordOrigin, cascades, BizzPermission.SHARE);
        for (Map.Entry<String, Set<ID>> e : cass.entrySet()) {
            log.info("Cascading share - {} > {}", e.getKey(), e.getValue());

            for (ID casid : e.getValue()) {
                affected += share(casid, toUserId, null, rights);
            }
        }

        if (countObservers() > 0 && shareChange) {
            notifyObservers(OperatingContext.create(currentUser, BizzPermission.SHARE, null, sharedAfter));
        }
        return affected;
    }

    @Override
    public int unshare(ID recordId, ID accessId) {
        final ID currentUser = getCurrentUser();

        Record unsharedBefore = null;
        if (countObservers() > 0) {
            unsharedBefore = EntityHelper.forUpdate(accessId, currentUser);
            unsharedBefore.setNull("belongEntity");
            unsharedBefore.setNull("recordId");
            unsharedBefore.setNull("shareTo");
            unsharedBefore = recordSnap(unsharedBefore, false);
        }

        delegateService.delete(accessId);

        if (countObservers() > 0) {
            notifyObservers(OperatingContext.create(currentUser, InternalPermission.UNSHARE, unsharedBefore, null));
        }
        return 1;
    }

    
    
    
    @Override
    public int bulk(BulkContext context) {
        BulkOperator operator = buildBulkOperator(context);
        try {
            return operator.exec();
        } catch (RebuildException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new RebuildException(ex);
        }
    }

    @Override
    public String bulkAsync(BulkContext context) {
        BulkOperator operator = buildBulkOperator(context);
        return TaskExecutors.submit(operator, context.getOpUser());
    }

    
    protected Map<String, Set<ID>> getCascadedRecords(ID mainRecordId, String[] cascadeEntities, Permission action) {
        if (cascadeEntities == null || cascadeEntities.length == 0) {
            return Collections.emptyMap();
        }

        final boolean fromTriggerNoFilter = GeneralEntityServiceContextHolder.isFromTrigger(false);

        Map<String, Set<ID>> entityRecordsMap = new HashMap<>();
        Entity mainEntity = MetadataHelper.getEntity(mainRecordId.getEntityCode());

        for (String cas : cascadeEntities) {
            if (!MetadataHelper.containsEntity(cas)) {
                log.warn("The entity not longer exists : {}", cas);
                continue;
            }

            Entity casEntity = MetadataHelper.getEntity(cas);
            Field[] reftoFields = MetadataHelper.getReferenceToFields(mainEntity, casEntity);
            if (reftoFields.length == 0) {
                log.warn("No any fields of refto found : {} << {}", cas, mainEntity.getName());
                continue;
            }

            List<String> or = new ArrayList<>();
            for (Field field : reftoFields) {
                or.add(String.format("%s = '%s'", field.getName(), mainRecordId));
            }

            String sql = String.format("select %s from %s where ( %s )",
                    casEntity.getPrimaryField().getName(), casEntity.getName(),
                    StringUtils.join(or.iterator(), " or "));

            Object[][] array;
            if (fromTriggerNoFilter) {
                array = Application.createQueryNoFilter(sql).array();
            } else {
                Filter filter = Application.getPrivilegesManager().createQueryFilter(getCurrentUser(), action);
                array = Application.getQueryFactory().createQuery(sql, filter).array();
            }

            Set<ID> records = new HashSet<>();
            for (Object[] o : array) {
                records.add((ID) o[0]);
            }
            entityRecordsMap.put(cas, records);
        }
        return entityRecordsMap;
    }

    
    private BulkOperator buildBulkOperator(BulkContext context) {
        if (context.getAction() == BizzPermission.DELETE) {
            return new BulkDelete(context, this);
        } else if (context.getAction() == BizzPermission.ASSIGN) {
            return new BulkAssign(context, this);
        } else if (context.getAction() == BizzPermission.SHARE) {
            return new BulkShare(context, this);
        } else if (context.getAction() == InternalPermission.UNSHARE) {
            return new BulkUnshare(context, this);
        } else if (context.getAction() == BizzPermission.UPDATE) {
            return new BulkBatchUpdate(context, this);
        } else if (context.getAction() == InternalPermission.APPROVAL) {
            return (BulkOperator) ReflectUtils.newObject(
                    "com.rebuild.rbv.approval.BulkBatchApprove", context, this);
        }

        throw new UnsupportedOperationException("Unsupported bulk action : " + context.getAction());
    }

    
    protected boolean checkModifications(Record record, Permission action) throws DataSpecificationException {
        final Entity entity = record.getEntity();
        final Entity mainEntity = entity.getMainEntity();

        if (action == BizzPermission.CREATE) {
            
            
            if (mainEntity != null && MetadataHelper.hasApprovalField(mainEntity)) {
                Field dtmField = MetadataHelper.getDetailToMainField(entity);
                ID dtmFieldValue = record.getID(dtmField.getName());
                if (dtmFieldValue == null) {
                    throw new DataSpecificationException(Language.L("%s 不允许为空", EasyMetaFactory.getLabel(dtmField)));
                }

                ApprovalState state = ApprovalHelper.getApprovalState(dtmFieldValue);
                if (state == ApprovalState.APPROVED || state == ApprovalState.PROCESSING) {
                    throw new DataSpecificationException(state == ApprovalState.APPROVED
                            ? Language.L("主记录已完成审批，不能添加明细")
                            : Language.L("主记录正在审批中，不能添加明细"));
                }
            }

        } else {
            final Entity checkEntity = mainEntity != null ? mainEntity : entity;
            ID checkRecordId = record.getPrimary();

            if (checkEntity.containsField(EntityHelper.ApprovalId)) {
                
                String recordType = Language.L("记录");
                if (mainEntity != null) {
                    checkRecordId = QueryHelper.getMainIdByDetail(checkRecordId);
                    recordType = Language.L("主记录");
                }

                ApprovalState currentState;
                ApprovalState changeState = null;
                try {
                    currentState = ApprovalHelper.getApprovalState(checkRecordId);
                    if (record.hasValue(EntityHelper.ApprovalState)) {
                        changeState = (ApprovalState) ApprovalState.valueOf(record.getInt(EntityHelper.ApprovalState));
                    }

                } catch (NoRecordFoundException ignored) {
                    log.warn("No record found for check ({}) : {}", action.getName(), checkRecordId);
                    return false;
                }

                boolean unallow = false;
                if (action == BizzPermission.DELETE) {
                    unallow = currentState == ApprovalState.APPROVED || currentState == ApprovalState.PROCESSING;
                } else if (action == BizzPermission.UPDATE) {
                    unallow = currentState == ApprovalState.APPROVED || currentState == ApprovalState.PROCESSING;

                    
                    if (unallow) {
                        boolean adminCancel = currentState == ApprovalState.APPROVED && changeState == ApprovalState.CANCELED;
                        if (adminCancel) unallow = false;
                    }

                    
                    if (unallow) {
                        boolean forceUpdate = GeneralEntityServiceContextHolder.isAllowForceUpdateOnce();
                        if (forceUpdate) unallow = false;
                    }
                }

                if (unallow) {
                    if (RobotTriggerObserver.getTriggerSource() != null) {
                        recordType = Language.L("关联记录");
                    }

                    throw new DataSpecificationException(currentState == ApprovalState.APPROVED
                            ? Language.L("%s已完成审批，禁止操作", recordType)
                            : Language.L("%s正在审批中，禁止操作", recordType));
                }
            }
        }

        if (action == BizzPermission.CREATE || action == BizzPermission.UPDATE) {
            
        }

        return true;
    }

    
    private void appendDefaultValue(Record recordOfNew) {
        Assert.isNull(recordOfNew.getPrimary(), "Must be new record");

        Entity entity = recordOfNew.getEntity();
        if (MetadataHelper.isBizzEntity(entity) || !MetadataHelper.hasPrivilegesField(entity)) {
            return;
        }

        for (Field field : entity.getFields()) {
            if (MetadataHelper.isCommonsField(field)
                    || recordOfNew.hasValue(field.getName(), true)) {
                continue;
            }

            Object defaultValue = EasyMetaFactory.valueOf(field).exprDefaultValue();
            if (defaultValue != null) {
                recordOfNew.setObjectValue(field.getName(), defaultValue);
            }
        }
    }

    
    private void setSeriesValue(Record record) {
        boolean skip = GeneralEntityServiceContextHolder.isSkipSeriesValue(false);
        Field[] seriesFields = MetadataSorter.sortFields(record.getEntity(), DisplayType.SERIES);

        for (Field field : seriesFields) {
            
            if (record.hasValue(field.getName()) && skip) {
                continue;
            }

            record.setString(field.getName(), SeriesGeneratorFactory.generate(field, record));
        }
    }

    @Override
    public List<Record> getAndCheckRepeated(Record checkRecord, int limit) {
        final Entity entity = checkRecord.getEntity();

        List<String> checkFields = new ArrayList<>();
        for (Iterator<String> iter = checkRecord.getAvailableFieldIterator(); iter.hasNext(); ) {
            Field field = entity.getField(iter.next());
            if (field.isRepeatable()
                    || !checkRecord.hasValue(field.getName(), false)
                    || MetadataHelper.isCommonsField(field)
                    || EasyMetaFactory.getDisplayType(field) == DisplayType.SERIES) {
                continue;
            }
            checkFields.add(field.getName());
        }

        if (checkFields.isEmpty()) return Collections.emptyList();

        
        final String orAnd = StringUtils.defaultString(
                EasyMetaFactory.valueOf(entity).getExtraAttr(EasyEntityConfigProps.REPEAT_FIELDS_CHECK_MODE), "or");

        StringBuilder checkSql = new StringBuilder("select ")
                .append(entity.getPrimaryField().getName()).append(", ")  
                .append(StringUtils.join(checkFields.iterator(), ", "))
                .append(" from ")
                .append(entity.getName())
                .append(" where ( ");
        for (String field : checkFields) {
            checkSql.append(field).append(" = ? ").append(orAnd).append(" ");
        }
        checkSql.delete(checkSql.lastIndexOf("?") + 1, checkSql.length()).append(" )");

        
        if (checkRecord.getPrimary() != null) {
            checkSql.append(String.format(" and (%s <> '%s')",
                    entity.getPrimaryField().getName(), checkRecord.getPrimary()));
        }

        
        if (entity.getMainEntity() != null) {
            String globalRepeat = EasyMetaFactory.valueOf(entity).getExtraAttr(EasyEntityConfigProps.DETAILS_GLOBALREPEAT);
            
            if (!BooleanUtils.toBoolean(globalRepeat)) {
                String dtf = MetadataHelper.getDetailToMainField(entity).getName();
                ID mainid = checkRecord.getID(dtf);
                if (mainid == null) {
                    log.warn("Check all records of detail for repeatable");
                } else {
                    checkSql.append(String.format(" and (%s = '%s')", dtf, mainid));
                }
            }
        }

        Query query = ((BaseService) delegateService).getPersistManagerFactory().createQuery(checkSql.toString());

        int index = 1;
        for (String field : checkFields) {
            query.setParameter(index++, checkRecord.getObjectValue(field));
        }
        return query.setLimit(limit).list();
    }

    @Override
    public void approve(ID recordId, ApprovalState state, ID approvalUser) {
        Assert.isTrue(
                state == ApprovalState.REVOKED || state == ApprovalState.APPROVED,
                "Only REVOKED or APPROVED allowed");

        if (approvalUser == null) {
            approvalUser = UserService.SYSTEM_USER;
            log.warn("Use '{}' do approve : {}", approvalUser, recordId);
        }

        Record approvalRecord = EntityHelper.forUpdate(recordId, approvalUser, false);
        approvalRecord.setInt(EntityHelper.ApprovalState, state.getState());
        delegateService.update(approvalRecord);

        

        RobotTriggerManual triggerManual = new RobotTriggerManual();

        

        TriggerAction[] hasTriggers = getSpecTriggers(approvalRecord.getEntity().getDetailEntity(), null,
                state == ApprovalState.APPROVED ? TriggerWhen.APPROVED : TriggerWhen.REVOKED);
        if (hasTriggers.length > 0) {
            for (ID did : QueryHelper.detailIdsNoFilter(recordId)) {
                Record dAfter = EntityHelper.forUpdate(did, approvalUser, false);

                if (state == ApprovalState.REVOKED) {
                    triggerManual.onRevoked(
                            OperatingContext.create(approvalUser, InternalPermission.APPROVAL, null, dAfter));
                } else {
                    triggerManual.onApproved(
                            OperatingContext.create(approvalUser, InternalPermission.APPROVAL, null, dAfter));
                }
            }
        }

        Record before = approvalRecord.clone();
        if (state == ApprovalState.REVOKED) {
            before.setInt(EntityHelper.ApprovalState, ApprovalState.APPROVED.getState());
            triggerManual.onRevoked(
                    OperatingContext.create(approvalUser, InternalPermission.APPROVAL, before, approvalRecord));
        } else {
            before.setInt(EntityHelper.ApprovalState, ApprovalState.PROCESSING.getState());
            triggerManual.onApproved(
                    OperatingContext.create(approvalUser, InternalPermission.APPROVAL, before, approvalRecord));
        }

        
        new RevisionHistoryObserver().onApprovalManual(
                OperatingContext.create(approvalUser, InternalPermission.APPROVAL, before, approvalRecord));
    }

    
    private TriggerAction[] getSpecTriggers(Entity entity, ActionType specType, TriggerWhen... when) {
        if (entity == null || when.length == 0) return new TriggerAction[0];

        TriggerAction[] triggers = RobotTriggerManager.instance.getActions(entity, when);
        if (triggers.length == 0 || specType == null) return triggers;

        List<TriggerAction> specTriggers = new ArrayList<>();
        for (TriggerAction t : triggers) {
            if (t.getType() == specType) specTriggers.add(t);
        }
        return specTriggers.toArray(new TriggerAction[0]);
    }

    @Override
    public String toString() {
        return getEntityCode() + "#" + super.toString();
    }
}