/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.files;

import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.privileges.UserHelper;
import com.rebuild.utils.JSONUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.map.LRUMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


public class FilesHelper {

    
    public static final String SCOPE_ALL = "ALL";
    
    public static final String SCOPE_SELF = "SELF";

    private static final LRUMap<String, Integer> FILESIZES = new LRUMap<>(2000);

    
    public static void storeFileSize(String filePath, int fileSize) {
        FILESIZES.put(filePath, fileSize);
    }

    
    public static Record createAttachment(String filePath, ID user) {
        Record attach = EntityHelper.forNew(EntityHelper.Attachment, user);
        attach.setString("filePath", filePath);

        String ext = FilenameUtils.getExtension(filePath);
        if (StringUtils.isNotBlank(ext)) {
            if (ext.length() > 10) ext = ext.substring(0, 10);
            attach.setString("fileType", ext);
        }

        if (FILESIZES.containsKey(filePath)) {
            attach.setInt("fileSize", FILESIZES.remove(filePath));
        } else {
            Object[] db = Application.createQueryNoFilter(
                    "select fileSize from Attachment where filePath = ?")
                    .setParameter(1, filePath)
                    .unique();
            if (db != null) {
                attach.setInt("fileSize", (Integer) db[0]);
            }
        }

        return attach;
    }

    
    public static JSONArray getAccessableFolders(ID user, ID parent) {
        
        String sql = "select folderId,name,scope,createdBy,parent,scope from AttachmentFolder where ";
        if (parent == null) sql += "parent is null";
        else sql += String.format("parent = '%s'", parent);

        sql += " order by name";
        Object[][] array = Application.createQueryNoFilter(sql).array();

        JSONArray folders = new JSONArray();
        for (Object[] o : array) {
            boolean access = SCOPE_ALL.equals(o[2]) || user.equals(o[3]);
            final String scopeSpecUsers = o[2] != null && o[2].toString().length() >= 20 ? o[2].toString() : null;
            
            if (!access && scopeSpecUsers != null) {
                Collection<String> c = new HashSet<>();
                CollectionUtils.addAll(c, scopeSpecUsers.split(","));
                Set<ID> inUsers = UserHelper.parseUsers(c, null);
                access = inUsers.contains(user);
            }

            
            if (!access) continue;

            o[2] = SCOPE_SELF.equals(o[2]);
            o[3] = user.equals(o[3]) || UserHelper.isAdmin(user);  
            o[5] = scopeSpecUsers;
            JSONObject folder = JSONUtils.toJSONObject(
                    new String[] { "id", "text", "private", "self", "parent", "specUsers" }, o);

            JSONArray children = getAccessableFolders(user, (ID) o[0]);
            if (!children.isEmpty()) {
                folder.put("children", children);
            }
            folders.add(folder);
        }
        return folders;
    }

    
    public static Set<ID> getAccessableFolders(ID user) {
        JSONArray accessable = getAccessableFolders(user, null);
        Set<ID> set = new HashSet<>();
        intoAccessableFolders(accessable, set);
        return set;
    }

    private static void intoAccessableFolders(JSONArray folders, Set<ID> into) {
        for (Object o : folders) {
            JSONObject folder = (JSONObject) o;
            into.add(ID.valueOf(folder.getString("id")));

            JSONArray c = folder.getJSONArray("children");
            if (c != null) intoAccessableFolders(c, into);
        }
    }

    
    public static Set<ID> getChildFolders(ID parent) {
        Object[][] array = Application.createQueryNoFilter(
                "select folderId,createdBy from AttachmentFolder where parent = ?")
                .setParameter(1, parent)
                .array();
        if (array.length == 0) return Collections.emptySet();

        Set<ID> set = new HashSet<>();
        for (Object[] o : array) {
            set.add((ID) o[0]);
            set.addAll(getChildFolders((ID) o[0]));
        }
        return set;
    }

    
    public static boolean isFileManageable(ID user, ID fileId) {
        return UserHelper.isAdmin(user) || UserHelper.isSelf(user, fileId);
    }

    
    public static boolean isFileAccessable(ID user, ID fileId) {
        Object[] o = Application.getQueryFactory().uniqueNoFilter(fileId, "folderId");
        if (o == null) return true;
        return getAccessableFolders(user).contains((ID) o[0]);
    }
}
