/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.privileges.bizz;


public enum ZeroEntry {

    
    AllowLogin(true),
    
    AllowBatchUpdate(false),
    
    AllowDataImport(false),
    
    AllowDataExport(false),
    
    AllowCustomNav(true),
    
    AllowCustomDataList(true),
    
    AllowCustomChart(true),
    
    AllowNoDesensitized(false),
    
    AllowAtAllUsers(false),

    
    EnableBizzPart(false),

    
    AllowRevokeApproval(false),

    
    AllowRecordMerge(false),

    ;

    private final boolean defaultVal;

    ZeroEntry(boolean defaultVal) {
        this.defaultVal = defaultVal;
    }

    
    public boolean getDefaultVal() {
        return defaultVal;
    }
}
